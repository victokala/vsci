<?php

use PHPUnit\Framework\TestCase;

class ValidatePasswordTest extends TestCase {

    public function testValidLength(){
        $valpass = new ValidatePassword();
        $this->assertFalse($valpass->validLength('1234'));
    }
}